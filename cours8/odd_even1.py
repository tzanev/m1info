"""
This script checks if a number is even or odd.

Usage:
  odd_even.py [--] <number>

Options:
  -h --help     Display this help message
  <number>      The number to check
"""

from docopt import docopt

arguments = docopt(__doc__)
try:
    number = int(arguments["<number>"])
except ValueError:
    print("The number must be an integer")
    print(__doc__)
    exit(1)

if number % 2 == 0:
    print(f"{number} is even")
else:
    print(f"{number} is odd")
