#!/usr/bin/env python
"""Usage:
  table.py [--format=<f>] <number>

Print the multiplication table up to <number> in the format <f>
  
Options:
    -h --help     Display this help message
    <number>      The maximal number in the table
    --format=<f>  The format of the numbers, one of d, x, o, b [default: d]
"""

from docopt import docopt


def table(n, f="d"):
    # on calcule la largeur maximale des nombres de la table
    # avec le séparateur '|' de droite
    m = len(f"{n*n:{f}}|")
    # la fonction qui convertit un nombre au format f
    # et rajoute le séparateur | à droite
    tocell = lambda x: f"{x:{m}{f}}|"
    # calcul de la ligne horizontale
    # il y a n colonnes de largeur m+1, et 1 pour le | de début
    hline = "-" * (n * (m + 1) + 1)
    print(hline)
    for i in range(1, n + 1):
        print("|" + "".join([tocell(i * j) for j in range(1, n + 1)]))
        print(hline)


if __name__ == "__main__":
    # on récupère les arguments
    arguments = docopt(__doc__)

    # on vérifie que <number> est bien un entier
    try:
        number = int(arguments["<number>"])
    except ValueError:
        print("The number must be an integer")
        print(__doc__)
        exit(1)

    # on vérifie que le format est correct
    format = arguments["--format"]
    if format not in "dxob":
        print("The format must be one of d, x, o, b")
        print(__doc__)
        exit(1)

    # on imprime la table
    table(number, format)
