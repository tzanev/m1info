import sys

if len(sys.argv) != 2:
    print("Usage: odd_even.py <number>")
    exit(1)

try:
    number = int(sys.argv[1])
except ValueError:
    print("The number must be an integer")
    exit(1)

if number % 2 == 0:
    print(f"{number} is even")
else:
    print(f"{number} is odd")
