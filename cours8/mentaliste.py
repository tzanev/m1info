#!/usr/bin/env python
"""
Jeu de devinette de nombre : l'utilisateur joue contre l'ordinateur.
Chaque joueur doit deviner un nombre entre 1 et <maxnumber> inclus.
Le jeu se déroule comme suit :
1. Chaque joueur choisit un nombre entre 1 et <maxnumber> ; l'ordinateur choisit un nombre au hasard.
2. À chaque tour, les joueurs sont informés si leur nombre est trop élevé, trop bas ou s'ils ont gagné.
3. Le jeu se termine lorsqu'un des joueurs a trouvé le nombre.

Usage:
    mentaliste.py [--max=<nombre>]
    mentaliste.py (-h | --help)

Options:
    --max=<nombre>  Le nombre maximum à deviner [default: 100]
    -h --help       Afficher ce message d'aide

"""

from docopt import docopt
from random import randint
import sys
from readchar import readkey


def guess_number(maxnumber):
    """La fonction principale du jeu"""

    computer_number = get_computer_number(maxnumber)

    user_win, computer_win = False, False
    while not user_win and not computer_win:
        print("\n------- Ton tour -------")
        user_win = user_guess(computer_number)
        print("\n------ Mon tour -------")
        computer_win = computer_guess()

    print("\n------ Fin du jeu -------")
    if user_win and computer_win:
        print("Nous avons trouvé le nombre en même temps! Égalité!")
    elif user_win:
        print("Tu as trouvé mon nombre en premier! Tu as gagné!")
    else:
        print("J'ai trouvé ton nombre en premier! J'ai gagné!")
        print(f"Mon nombre était {computer_number}.")


def user_guess(computer_number):
    """
    L'utilisateur devine le nombre de l'ordinateur.
    Affiche un message en fonction, après la réponse de l'utilisateur.
    Retourne True si l'utilisateur a trouvé le nombre, False sinon.
    """

    print("C'est à ton tour de deviner.")
    while True:
        try:
            guess = int(input("Ton hypothèse : "))
            break
        except ValueError:
            print("Entrez un nombre valide ou Ctrl+C pour quitter.")
        except KeyboardInterrupt:
            print("\nTu abandonnes...tu as perdu!")
            sys.exit(1)

    if guess == computer_number:
        print("Tu as trouvé mon nombre! Bravo!")
        return True
    elif guess < computer_number:
        print("Trop petit!")
    else:
        print("Trop grand!")
    return False


# L'historique de l'ordinateur
computer_hist = {
    "min": None,  # le nombre minimum possible
    "max": None,  # le nombre maximum possible
}


def get_computer_number(maxnumber):
    """
    L'ordinateur choisit un nombre au hasard entre 1 et <maxnumber>.
    Initialise l'historique de l'ordinateur.
    Affiche un message pour informer l'utilisateur.
    Retourne le nombre choisi.
    """

    # initialize l'historique
    computer_hist["min"] = 1
    computer_hist["max"] = maxnumber

    # Choix du nombre
    number = randint(computer_hist["min"], computer_hist["max"] + 1)
    print(f"J'ai choisi un nombre entre 1 et {maxnumber}.")
    print("Tu dois faire la même chose et le garder en tête.")
    return number


def get_key(allowed_keys):
    """
    Lit une touche du clavier et retourne la touche si elle est dans la chaîne `allowed_keys`.
    Sinon, affiche un message d'erreur et redemande une touche.
    """

    while True:
        k = readkey().lower()
        if k in allowed_keys:
            return k
        print(k)
        print("Touche invalide.")
        print(f"Les touches autorisées sont : {allowed_keys} ou Ctrl+C pour quitter.")


def computer_guess():
    """
    L'ordinateur devine le nombre de l'utilisateur en fonction de l'historique.
    """

    # faire une hypothèse
    guess = int((computer_hist["min"] + computer_hist["max"]) / 2)
    print(f"C'est mon tour, je pense que ton nombre est {guess}.")

    # demander à l'évaluation à l'utilisateur
    print(f"Est-ce trop [p]etit, trop [g]rand ou [e]xacte ?", end=" ", flush=True)
    try:
        answer = get_key("peg")
        print(answer)
    except KeyboardInterrupt:
        print("\nTu abandonnes...tu as perdu!")
        sys.exit(1)

    # mettre à jour l'historique en fonction de la réponse (si non exacte)
    if answer == "e":
        return True
    elif answer == "p":
        computer_hist["min"] = guess + 1
    else:
        computer_hist["max"] = guess - 1

    # vérifier si l'historique est cohérent
    if computer_hist["min"] > computer_hist["max"]:
        print(
            f"Tu triches! Ton nombre ne peut pas être entre {computer_hist['min']} et {computer_hist['max']}."
        )
        sys.exit(1)

    return False


# Début du jeu
if __name__ == "__main__":
    # lire les arguments
    arguments = docopt(__doc__)
    try:
        maxnumber = int(arguments["--max"])
    except ValueError:
        print("Entrez un nombre valide pour --max.")
        print(__doc__)
        sys.exit(1)

    # lance le jeu
    guess_number(maxnumber)
