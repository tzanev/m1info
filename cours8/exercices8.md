# Exercices (séance 8)

## Exemple

Le fichier [mentaliste.py](mentaliste.py) dans ce répertoire est un script qui permet de jouer à « deviner un nombre ».

## Exercice 1

Le bût de cet exercice est de créer le jeu « Pierre - Feuille - Ciseaux ».

Le jeu doit être implémenté dans un fichier `pfs.py`.
Voici un exemple possible de l'interface du jeu en `docopt` :

```python
"""
Il s'agit d'un jeu de pierre-papier-ciseaux simple où l'utilisateur joue contre l'ordinateur.
L'ordinateur choisit un mouvement au hasard et l'utilisateur doit en choisir un également.
Le jeu se termine lorsqu'un des joueurs a remporté <rounds> tours.

Usage:
    pfs.py [--tours=<nombre>] [--effacer]
    pfs.py (-h | --help)

Options:
    --tours=<nombre>  Le nombre de tours à jouer [default: 7]
    --effacer         Effacer l'écran entre les tours
    -h --help         Afficher ce message d'aide
"""
```

Voici aussi un exemple d'utilisation du jeu :

```bash
> python .\pfs.py --tours 1

------- Tour 1 de 1 -------
Choisis [p]ierre, [f]euille ou [c]iseaux : p
Tu as choisi 👊
J'ai choisi ✌️
Tu as gagné!
Résultat : toi 1 - moi 0

------- Fin du jeu -------
Tu as gagné!
```

## Exercice 2

Créer un script `pendu.py` qui implémente le jeu du pendu.
Pour le choix du mot vous pouvez utiliser le fichier [mots.txt](mots.txt) dans ce répertoire.

Voici un exemple de l'interface du jeu possible en `docopt` :

```python
"""
Le pendu : le joueur doit deviner un mot choisi par l'ordinateur.

Usage:
    pendu.py [options]
    pendu.py (-h | --help)

Options:
    --min=<number>     La longueur minimale du mot à deviner [default: 5]
    --max=<number>     La longueur maximale du mot à deviner [default: 20]
    --errors=<number>  Le nombre d'erreurs maximum [default: 7]
    -h --help          Affiche ce message d'aide
"""
```

Voici un exemple d'utilisation du jeu :

```bash
> python pendu.py --max 11 --errors 5
+---+
|   |
|   o
|
|
|
========
Le mot à deviner a 7 lettres (sans les accents).
Vous avez droit à 5 erreurs.
_______
Entrez une lettre:
```

## Exercice 3

Créer un script `demineur.py` qui implémente le jeu du démineur.

Voici un exemple de l'interface du jeu possible en `docopt` :

```python
"""
Démineur : le joueur doit déminer une grille de cases piégées.

Usage:
    demineur.py [--size=<number>] [--mines=<number>]
    demineur.py (-h | --help)

Options:
    --size=<number>   La taille de la grille [default: 10]
    --mines=<number>  Le nombre de mines [default: 10]
    -h --help         Affiche ce message d'aide
"""
```

Voici un exemple d'utilisation du jeu :

```bash
> python demineur.py --size 5 --mines 3
    1  2  3  4  5
A  🔲 🔲 🔲 🔲 🔲
B  🔲 🔲 🔲 🔲 🔲
C  🔲 🔲 🔲 🔲 🔲
D  🔲 🔲 🔲 🔲 🔲
E  🔲 🔲 🔲 🔲 🔲
Nombre de mines restantes : 3
Vérifier une case (ex. A3) ou marquer une mine (ex. *F4).
Pour quitter, entrez q ou quit.
Choisir : A3
```
