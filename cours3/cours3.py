# %% [markdown]
# # Types énumérables et boucles
#
# Dans ce chapitre, nous allons voir comment parcourir les types énumérables (listes, tuples, chaînes de caractères, etc.) et comment les manipuler.
#

# %% [markdown]
# ## Listes

# %% [markdown]
# # exemple d'une liste
# %%
list1 = ["abcd", 786, 2.23, "john", 70.3]
print(list1)

# %% [markdown]
# # accès aux éléments de la liste
# %%
print(list1[0])
print(list1[1:3])

# %% [markdown]
# # longueur de la liste
# %%
print(len(list1))

# %% [markdown]
# # Ajout d'éléments
# %%
list1.append("ajout")
print(list1)

# %% [markdown]
# # Suppression d'éléments
# %%
del list1[2]
print(list1)

# %% [markdown]
# # Concaténation
# %%
list2 = ["autre", "liste"]
list3 = list1 + list2
print(list3)

# %% [markdown]
# # Copie
# %%
print(f"liste 3 avant : {list3}")
list4 = list3
list4[0] = "nouveau"
print(f"liste 3 après : {list3}")
list4 = list3.copy()  # ou list3[:]
list4[0] = "vraiment nouveau"
print(f"liste 4 modifiée : {list4}")
print(f"liste 3 inchangée : {list3}")

# %% [markdown]
# # Itération (boucle for, enumerate)

# %% [markdown]
# ## Boucle for simple
# %%
for element in list3:
    print(element)

# %% [markdown]
# ## Boucle for avec indice
# %%
for i, element in enumerate(list3):
    print(f"élément {i} : {element}")

# %% [markdown]
# ## Boucle for sur les indices
# %%
for i in range(len(list3)):
    print(f"élément {i} : {list3[i]}")

# %% [markdown]
# ## Boucle while
# %%
i = 0
while i < len(list3):
    print(f"élément {i} : {list3[i]}")
    i += 1

# %% [markdown]
# # Compréhension (transformation et/ou extraction de liste)

# %% [markdown]
# ## transformation
# %%
print(list1)
list5 = [str(e) for e in list1]
print(list5)

# la même chose avec une boucle for (à ne pas faire)
list5 = []
for e in list1:
    list5.append(str(e))
print(list5)

# %% [markdown]
# ## extraction (filtrage)
# %%
list5 = [e for e in list1 if isinstance(e, str)]
print(list5)

# %% [markdown]
# ## les deux en même temps
# %%
list5 = [str(e) for e in list1 if not isinstance(e, str)]
print(list5)

# %% [markdown]
# # Tri (avec et sans modification de la liste)
# %%
list5 = [str(e) for e in list1]
print("triée :", sorted(list5))
print("triée inversement :", sorted(list5, reverse=True))
print("non modifiée :", list5)
list5.sort()
print("modifiée (triée) :", list5)
list5.sort(reverse=True)
print("modifiée (triée inversement) :", list5)

# %% [markdown]
# # Recherche
# %%
print("john" in list5)
print(list5.index("john"))

# %% [markdown]
# # Conversion en chaîne de caractères
# %%
str5 = " : ".join(list5)
print(str5)

# %% [markdown]
# # Conversion en tuple, ensemble, dictionnaire, etc.
# %%
tuple5 = tuple(list5)
print(tuple5)

set5 = set(list5)
print(set5)

dict5 = dict(enumerate(list5))
print(dict5)

# %% [markdown]
# # Déstructuration (unpacking)
# %%
print(list5)
debut, *milieu, fin = list5
print(debut)  # list5[0]
print(milieu)  # liste5[1:-1]
print(fin)  # liste5[-1]

# %% [markdown]
# # Tuples
# Les tuples sont des listes non modifiables (on dit qu'ils sont immuables).
# Ils se manipulent de la même manière que les listes, sauf qu'on ne peut pas les modifier.
#
# %%
tuple1 = ("abcd", 786, 2.23, "john", 70.2)
print(tuple1)
print(tuple1[0])
print(tuple1[1:3])
print(len(tuple1))
print(tuple1 + ("ajout",))
print(sorted(str(e) for e in tuple1))

# %% [markdown]
# # Dictionnaires
# %% [markdown]
# # exemple d'un dictionnaire (ch'ticonnaire)
# %%
chti = {"mon": "min", "à refaire": "arfaire", "les siens": "el chiens"}
print(chti)

# %% [markdown]
# # Accès aux éléments
# %%
print(f"« mon » en ch'ti : {chti['mon']}")
print(f"« dans » en ch'ti : {chti.get('dans', '''ch'pa...dins ?''')}")


# %% [markdown]
# # Ajout d'éléments
# %%
chti["rouge"] = "rouche"
print(chti)

# %% [markdown]
# # Suppression d'éléments
# %%
del chti["rouge"]
print(chti)

# %% [markdown]
# # Itération
# %%
# par clé
for cle in chti:
    print(f"« {cle} » en ch'ti : {chti[cle]}")

# %%
# ou avec .keys()
for cle in chti.keys():
    print(f"« {cle} » en ch'ti : {chti[cle]}")

# %%
# par clé et valeur
for cle, valeur in chti.items():
    print(f"« {cle} » en ch'ti : {valeur}")

# %%
# par valeur
for valeur in chti.values():
    print(f"« {valeur} » est un mot en ch'ti")

# %% [markdown]
# Compréhension
# %%
# transformation
chtimaj = {cle: valeur.upper() for cle, valeur in chti.items()}
print(chtimaj)
# %%
# conversion en liste
chtilist = [valeur for cle, valeur in chti.items()]
print(chtilist)
# ou avec .values()
chtilist = list(chti.values())
print(chtilist)
# %%
# conversion en ensemble (sans doublons)
chtiset = {valeur for cle, valeur in chti.items()}
print(chtiset)
# %%
# conversion d'une liste en dictionnaire
carres = {i: i**2 for i in range(10)}
for cle, valeur in carres.items():
    print(f"{cle}² = {valeur}")

# %% [markdown]
# ## Tri (sans modification du dictionnaire)
# Ce n'est pas possible de trier un dictionnaire directement.
# Il faut d'abord le convertir en liste de tuples.
# %%
# trie par clé
print(dict(sorted(chti.items())))
# %%
# trie par valeur
print(dict(sorted(chti.items(), key=lambda x: x[1])))
# %%
# trie par clé en ignorant les accents
from unidecode import unidecode

print(dict(sorted(chti.items(), key=lambda x: unidecode(x[0]))))

# %% [markdown]
# ## Boucles particulières
# %%
import math, random

# %% [markdown]
# ### boucle for avec else
# La clause else n'a de sens que si la boucle est interrompue (par un break, return ou une exception).
# %%
for i in range(10):
    v = random.randint(0, 100)
    # vérifie si c'est un carré
    if v == int(math.sqrt(v)) ** 2:
        print(f"{v} est un carré")
        break
    else:
        print(f"{v} n'est pas un carré")
else:
    print("pas de carré trouvé dans les 10 premiers nombres aléatoires")
# %% [markdown]
# ### boucle while avec else
# %%
sum = 0
while sum < 200:
    v = random.randint(0, 100)
    # vérifie si c'est un carré
    if v == int(math.sqrt(v)) ** 2:
        print(f"{v} est un carré")
        break
    else:
        print(f"{v} n'est pas un carré")
    sum += v
else:
    print("pas de carré trouvé avant d'atteindre 200, somme =", sum)

# %% [markdown]
# ### double boucle for
# %%
# table de multiplication
for i in range(5):
    for j in range(5):
        print(f"{i} × {j} = {i*j:>2}")

# %% [markdown]
# ### boucler sur deux listes en même temps
# %%
# zip
list1 = ["fruit", "légume", "viande", "poisson"]
list2 = ["pomme", "carotte", "poulet", "saumon"]
for e1, e2 in zip(list1, list2):
    print(f"{e1} : {e2}")

# %%
# la même chose de façon moins pythonesque
for i in range(len(list1)):
    print(f"{list1[i]} : {list2[i]}")
