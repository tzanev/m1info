# Séance 3: Types énumérables et boucles

Dans cette séance nous allons approfondir les notions de types énumérables et de boucles.
Les fichiers de cette séance sont:
- [cours3.ipynb](cours3.ipynb) : Le cours de la séance en version « notebook »
- [cours3.py](cours3.py) : Le même cours en version « script »
- [exercices3.ipynb](exercices3.ipynb) : Les exercices à rendre en fin de séance