# Séance 2: Programmation de Base en Python

**Objectif:** Explorer la programmation de base en Python et créer des scripts.

## Bases

Pour l'introduction aux variables, structures de contrôle et fonctions, se référer à la [cours2.ipynb](cours2.ipynb). Tout au long du cours, on approfondira ces notions.

## Modularité

Un module est un fichier contenant des fonctions et des variables.
Pour importer un module, on utilise `import`.

Voir les fichiers [hello.py](hello.py) et [test.py](test.py) pour un exemple.

## Création de scripts

En général un script est un fichier `.py` qui contient des instructions Python et qui souvent recoit des arguments en ligne de commande. Pour récupérer les arguments en ligne de commande, on utilise `sys.argv`:

Pour un exemple voir [hello.py](hello.py).

## Exercices

Voir le fichier [exercices2.ipynb](exercices2.ipynb).
