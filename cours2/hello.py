#!/usr/bin/env python
import sys


def hello(name):
    # si name commence par une majuscule
    if name[0].isupper():
        return f"Bonjour {name} !"
    else:
        return f"Salut {name} !"


if __name__ == "__main__":
    if len(sys.argv) > 1:
        print(hello(sys.argv[1]))
    else:
        print("Salut tout le monde !")
