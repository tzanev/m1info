# %% [markdown]
# # Fonctions et générateurs

# %% [markdown]
# ## Les fonctions en Python (avec def)
#
# Une fonction est un bloc de code qui effectue une tâche spécifique. Une fonction prend des arguments en entrée, effectue des opérations sur ces arguments, et renvoie un résultat (ou pas).
#
# En Python, on définit une fonction avec le mot-clé `def` suivi du nom de la fonction, suivi des arguments entre parenthèses et terminé par `:`. Le bloc de code qui constitue le corps de la fonction est indenté.
#
# Une fonction peut ne pas prendre d'argument et/ou ne pas renvoyer de résultat, auquel cas le résultat est `None`.
#


# %%
def print_hello():
    print("Bonjour tout le monde !")


# D'abord on appelle la fonction (qui affiche le message)
# pour former la chaîne de caractères, qui est ensuite affichée
print(f"Le résultat de 'print_hello' est : {print_hello()}")

# %% [markdown]
# Une fonction peut avoir deux type de paramètres :
# - les paramètres positionnels (sans valeur par défaut)
# - les paramètres nommés (avec valeur par défaut)
#
# Les paramètres nommés doivent être placés après les paramètres positionnels, aussi bien dans la définition de la fonction que lors de l'appel de la fonction.
#
# Un paramètre nommé peut être appelé sans nom si on respecte l'ordre des paramètres.
# Pour les paramètres nommés, on peut les appeler dans n'importe quel ordre.
#


# %%
def salutation(nom, prenom, formule="Bonjour", ponctuation=" !"):
    return f"{formule} {prenom} {nom}{ponctuation}"


print(salutation("Doe", "John"))
print(salutation("Doe", "John", "Hello"))
print(salutation("Doe", "John", formule="Salut"))
print(salutation("Doe", "John", ponctuation="."))
print(salutation("Doe", "John", ponctuation=" ?", formule="Coucou,"))

# %% [markdown]
# Une fonction est une variable comme une autre, on peut donc la passer en argument à une autre fonction, la stocker dans une variable, ou la retourner comme résultat d'une autre fonction.

# %%
hello = salutation
print(hello("Doe", "John"))


# %%
def maraba(nom, prenom):
    return f"Maraba {prenom} {nom} !"


# la variable salutation change de valeur pour devenir la fonction maraba
salutation = maraba
print(salutation("Doe", "John"))

# la variable hello reste inchangée et continue de référencer l'ancienne fonction salutation
print(hello("Doe", "John"))


# %%
# Une fonction peut retourner une autre fonction
def nouvelle_salutation(formule):
    def salutation(nom, prenom):
        return f"{formule} {prenom} {nom} !"

    return salutation


salutation = nouvelle_salutation("Здравей")

print(salutation("Doe", "John"))

# %% [markdown]
# ### Nombre indéfini d'arguments
#
# Il est possible de définir une fonction qui prend un nombre indéfini d'arguments. Pour cela, on utilise `*args` pour les arguments positionnels et `**kwargs` pour les arguments nommés. `args` et `kwargs` sont des conventions, on peut utiliser n'importe quel nom, mais il est recommandé de les utiliser pour faciliter la compréhension du code.


# %%
def print_arguments(*args, **kwargs):
    for i, arg in enumerate(args):
        print(f"Le paramètre {i} est : {arg}")
    for cle, valeur in kwargs.items():
        print(f"Le paramètre '{cle}' est : {valeur}")


print_arguments("Hello", "World", formule="Bonjour", ponctuation=" !")


# %%
def sum_of_squares(*args):
    return sum(arg**2 for arg in args)


print(sum_of_squares(1, 7, 35))

# %% [markdown]
# ## Les fonctions définies avec lambda
#
# En Python, on peut aussi définir des fonctions avec le mot-clé `lambda`. Ces fonctions sont des fonctions anonymes, c'est-à-dire qu'elles n'ont pas de nom. Elles sont souvent utilisées pour définir des fonctions simples qui ne sont utilisées qu'une seule fois.
#
# La syntaxe pour définir une fonction avec `lambda` est `lambda arguments: expression`. Le résultat de l'expression est renvoyé automatiquement (sans utiliser le mot-clé `return`).
#

# %%
bonjour = lambda nom, prenom: f"Bonjour {prenom} {nom} !"
print(bonjour("Doe", "John"))

# %% [markdown]
#
# Ces fonctions sont très pratiques pour les passer en argument à une autre fonction...
#


# %%
# une fonction qui prend une autre fonction en paramètre
def jaidit(f):
    print(f"J'ai dit : {f('Doe', 'John')}")


jaidit(bonjour)
jaidit(lambda nom, prenom: f"Salut {prenom} {nom} ;)")

# %%
# par exemple pour trier une liste de tuples,
# on peut utiliser une fonction lambda pour spécifier le critère de comparaison
sorted([(1, "youpi"), (2, "félicitations"), (3, "bravo")], key=lambda x: x[1])

# %% [markdown]
#
# ...ou pour les retourner comme résultat d'une autre fonction.

# %%
import math

# La première fonction lambda retourne une autre fonction lambda
sinus = lambda a=1: lambda x: math.sin(a * x)

# Le sinus qui prend un angle en degrés comme argument
sindeg = sinus(math.pi / 180)
print(sindeg(30))

# %% [markdown]
# ## Porté (scope) des variables
#
# Les variables définies dans une fonction sont locales à cette fonction, c'est-à-dire qu'elles ne sont accessibles que dans cette fonction. Les variables définies en dehors de toute fonction sont globales, c'est-à-dire qu'elles sont accessibles partout dans le programme.
#
# Si une variable locale a le même nom qu'une variable globale, la variable locale "cache" la variable globale. Pour accéder à la variable globale, on peut utiliser le mot-clé `global` suivi du nom de la variable.

# %%
# une variable globale
a = 1


def e(x):
    # on peut accéder à la variable globale en lecture
    return a * x


def f(x):
    # une variable locale qui masque la variable globale
    a = 2
    return a * x


def g(x):
    # on peut accéder à la variable globale en écriture avec le mot-clé 'global'
    global a
    a = 3
    return a * x


for func in (e, f, g):
    nom = func.__name__
    print(f"La valeur de {nom}(3) est {func(3)} et après la valeur de a est {a}.")

print("-" * 40)
a = 1

for funcname in ("e", "f", "g"):
    func = globals()[funcname]
    print(f"La valeur de {funcname}(3) est {func(3)} et après la valeur de a est {a}.")

# %% [markdown]
# ### Variables enfermées (enclosed variables)
#
# Si une fonction est définie dans une autre fonction, elle peut accéder aux variables de la fonction qui l'englobe. Ces variables sont appelées "enclosed variables". Ces variables sont accessibles même si la fonction qui les a définies n'est plus en cours d'exécution.


# %%
def entre(a, b):
    # les variables a et b sont capturées par la fonction lambda
    # elles sont donc accessibles même après la fin de la fonction entre
    c = min(a, b)
    d = max(a, b)
    return lambda x: c <= x <= d


# on considère qu'une valeur est admissible si elle est entre 5 et 3
admissible = entre(5, 3)

print(admissible(4))
print(admissible(7))


# %%
def compteur():
    # la variable n est locale à la fonction compteur
    # mais elle est non locale pour la fonction incrementer qui suit
    n = 0

    # la fonction incrementer modifie la variable capturée n
    # elle est retournée par la fonction compteur
    def incrementer():
        # la variable n est capturée par la fonction incrementer
        # il s'agit d'une variable non locale pour incrementer
        nonlocal n
        n += 1
        return n

    return incrementer


compte = compteur()  # compte <=> incrementer
print(compte())  # => incrémenter()
print(compte())  # => incrémenter()

# %% [markdown]
# # Gestion des erreurs
#
# En Python, on peut gérer les erreurs avec les mots-clés `try`, `except`, `else` et `finally`.
#
# Le bloc `try` contient le code qui peut générer une erreur. Si une erreur se produit, le bloc `except` est exécuté. Si aucune erreur ne se produit, le bloc `else` est exécuté. Le bloc `finally` est exécuté dans tous les cas, que des erreurs se soient produites ou non.
#
# On peut utiliser le mot-clé `raise` pour
# - lever une exception
# - relancer une exception
#
# On peut utiliser le mot-clé `assert` pour vérifier qu'une condition est vraie. Si la condition est fausse, une exception de type `AssertionError` est levée.

# %%
from random import choice

for i in range(10):
    a = choice([0, 1, 100])
    try:
        print(f"100 / {a} = {100/a}", end="")
    except ZeroDivisionError:
        print(f"100 / {a} = ∞", end="")
    else:
        print(" : division par un nombre différent de zéro", end=" ")
    finally:
        print(".")

# %% [markdown]
# ## Les générateurs
#
# Un générateur est une fonction qui au lieu d'utiliser le mot-clé `return` utilise le mot-clé `yield`. Une telle fonction s'arrête à chaque fois qu'elle rencontre un `yield` et renvoie le résultat. Lorsqu'on appelle la fonction à nouveau, elle reprend là où elle s'était arrêtée.
#
# On peut parcourir un générateur avec une boucle `for`, ou bien en utilisant la fonction `next` pour obtenir le résultat suivant.


# %%
# Un générateur fini sans paramètre
def generateur():
    print("🚩 Le générateur est démarré.")
    yield "🐪"
    yield "🐞"
    yield "🐍"
    print("🏁 Le générateur est terminé.")


print("-" * 40)
print("Création du générateur.")
gen = generateur()
print("Boucle sur le générateur.")
for m in gen:
    print(m)

print("-" * 40)
print("Création du générateur (bis).")
gen = generateur()
print("Quelques next() sur le générateur.")
print(next(gen))
print(next(gen))
print(next(gen))
# le suivant provoque une erreur (lève une exception)
print(next(gen))


# %%
# un générateur fini ou infini...that is the question !
def Syracuse(n):
    while n > 1:
        # retourne le nombre n et attend le prochain appel
        yield n
        if n % 2 == 0:
            n = n // 2
        else:
            n = 3 * n + 1
    # la suite de Syracuse se termine (n = 1, normalement)
    yield n


print(f"La suite de Syracuse(7):")
for i in Syracuse(7):
    print(i, end=", ")
print("\n")

# Quelques informations sur les premières suites de Syracuse
for i in range(1, 8):
    # s = list(Syracuse(i))
    s = [k for k in Syracuse(i)]
    print(
        f"Le temps de vol de Syracuse({i}) est {len(s)} et sa hauteur maximale est {max(s)}."
    )

# %%
# création d'un générateur fini
s = Syracuse(17)

# %%
# cellule à exécuter plusieurs fois
try:
    print(next(s))
except StopIteration:
    print("La suite de Syracuse est finie.")


# %%
# un générateur infini
def Fibonacci():
    a, b = 0, 1
    while True:
        yield a
        a, b = b, a + b


# on peut l'utiliser pour générer les termes de la suite de Fibonacci
for i, f in enumerate(Fibonacci()):
    print(f"Fibonacci({i}) = {f}")
    if i > 10:
        break

# %%
fib = Fibonacci()
# utilisation de next
# le début de la suite de Fibonacci
print("La début de la suite avec next :")
print(next(fib))
print(next(fib))
print(next(fib))
print(next(fib))
print("La suite avec une boucle for :")
for f in fib:
    print(f)
    if f > 10:
        break
print("Et on continue avec next :")
print(next(fib))
print(next(fib))
print(next(fib))
