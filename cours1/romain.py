# petit programme qui transforme un nombre romain en nombre arabe

# Déclaration des variables
# -------------------------
# dico des valeurs romaines
dico_romain = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}


# Déclaration des fonctions
# -------------------------
# fonction qui transforme un nombre romain en nombre arabe
def to_romain(s):
    # initialisation de le resultat
    res = 0

    # parcours de la chaine de caractère
    for i in range(len(s)):
        # récupération de la valeur du caractère
        v = dico_romain[s[i]]
        # si le caractère suivant est plus grand, on soustrait
        if i + 1 < len(s) and dico_romain[s[i + 1]] > v:
            res -= v
        else:
            res += v

    return res


print(to_romain("MCMXCIX"))
