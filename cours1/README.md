# Séance 1: Mise en Place de l'Environnement

- **Objectif:** Installer Python et VS Code/[Codium](https://github.com/VSCodium/vscodium/releases), explorer les extensions utiles.

1. Installer VS Codium ou VS Code ;
2. Installation de [mini conda](https://docs.conda.io/en/latest/miniconda.html) ;
  - Vérification que `conda` est bien installé :
    - `conda --version` ;
    - `conda list` ;
    - `conda env list` ;
    - `conda info` ;
  - Vérification que python est bien installé :
    - `python --version` ;
    - `pip --version` ;
3. Installation de quelques paquets python :
  - `conda install numpy matplotlib` ;
  - `conda install pandas` ;
  - `conda install jupyterlab` ;
4. Installation des extensions 

  Pour python :
  - Python (pour la prise en charge de python) ;
  - Pylance (pour l'analyse statique) ;
  - Jupyter (extension pack de Microsoft, pour l'interaction avec les notebooks) ;
  - Black Formatter (pour le formatage du code) ;
  - Github Copilot (pour écrire du code automatiquement) ;
  - Github Copilot Chat (pour des explications sur le code) ;

  Pour LaTeX :
  - Latex Language Support (pour la prise en charge de LaTeX de base) ;
  - LaTeX Workshop (pour une prise en charge plus avancée de LaTeX) ;

  Pour markdown :
  - Markdown All in One (pour la prise en charge de markdown) ;
  - Marp (pour faire des présentations en markdown) ;
 
  Extensions utiles :
  - Material Icon Theme (pour les icônes) ;
  - Calculate (pour faire des calculs dans l'éditeur) ;
  - French Language Pack (pour la correction orthographique en français) ;
  - Live Share (pour collaborer en temps réel) ;
  - Rainbow CSV (pour colorer les fichiers CSV) ;

5. Quelques sujets supplémentaires
  - Multicurseur
  - Expressions régulières
  - Terminal

