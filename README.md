# [<img src="https://about.gitlab.com/images/press/logo/svg/gitlab-logo-500.svg" height="50" style="vertical-align: middle">](https://gitlab.univ-lille.fr/tzanev/m1info)[m1info](https://gitlabpages.univ-lille.fr/tzanev/m1info)

Les documents du module « Informatique » du M1 Mathématiques à l'Université de Lille.

## 2025

- [Cours 1](cours1/)
  - [test1.py](cours1/test1.py) : un premier programme en python
  - [romain.py](cours1/romain.py) : un programme pour convertir des nombres romains en nombres arabes
- [Cours 2](cours2/)
  - [course2.ipynb](cours2/cours2.ipynb) : introduction à la programmation en python
  - [hello.py](cours2/hello.py) : un programme qui dit bonjour
  - [test.py](cours2/test.py) : un programme qui importe le module `hello`
  - [exercices2.ipynb](cours2/exercices2.ipynb) : des exercices « sans boucles »
- [Cours 3](cours3/)
  - [cours3.ipynb](cours3/cours3.ipynb) : les boucles en python (version « notebook »)
  - [cours3.py](cours3/cours3.py) : les boucles en python (version « script »)
  - [exercices3.ipynb](cours3/exercices3.ipynb) : des exercices sur les boucles
- [Cours 4](cours4/)
  - [cours4.ipynb](cours4/cours4.ipynb) : fonctions et générateurs(version « notebook »)
  - [cours4.py](cours4/cours4.py) : fonctions et générateurs (version « script »)
  - [exercices4.ipynb](cours4/exercices4.ipynb) : des exercices sur les fonctions et les générateurs

## 2024

Le cours de 2024 est disponible à l'adresse [gitlabpages.univ-lille.fr/tzanev/m1info/2024/](https://gitlabpages.univ-lille.fr/tzanev/m1info/2024/).
